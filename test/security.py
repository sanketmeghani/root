#!/usr/bin/env python3
import sys

if len(sys.argv) > 1:
    DOCKERFILE=sys.argv[1]
else:
    sys.exit("[e] Usage: "+sys.argv[0]+"[Dockerfile path]")

if 'user' not in open(DOCKERFILE).read():
    sys.exit("[e] process in the container does not run by a low privileged user")

if 'user: 0:0' in open(DOCKERFILE).read():
    sys.exit("[e] process in the container does not run by a low privileged user")

# use the following check for Dockerfile
#if 'USER root' in open(DOCKERFILE).read():
#    sys.exit("[e] process runs as root inside container")
#
#if 'USER' not in open(DOCKERFILE).read():
#    sys.exit("[e] process in the container does not run by a low privileged user")
#
#if 'adduser' not in open(DOCKERFILE).read():
#    sys.exit("[e] a user is not created for the process inside container. Use 'adduser'")
#
#if 'addgroup' not in open(DOCKERFILE).read():
#    sys.exit("[e] a group is not created for the user inside container. Use 'addgroup'")
#

